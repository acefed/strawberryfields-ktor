# StrawberryFields Ktor

- [2.0.0]
- [1.6.0] - 2024-08-10 - 9 files changed, 156 insertions(+), 129 deletions(-)
- [1.5.0] - 2024-03-10 - 3 files changed, 95 insertions(+), 29 deletions(-)
- [1.4.0] - 2024-02-04 - 6 files changed, 121 insertions(+), 68 deletions(-)
- [1.3.0] - 2023-11-11 - 6 files changed, 36 insertions(+), 25 deletions(-)
- [1.2.0] - 2023-11-05 - 10 files changed, 208 insertions(+), 90 deletions(-)
- [1.1.0] - 2023-08-19 - 2 files changed, 7 insertions(+), 7 deletions(-)
- 1.0.0 - 2023-08-16

[2.0.0]: https://gitlab.com/acefed/strawberryfields-ktor/-/compare/9a45a65b...main
[1.6.0]: https://gitlab.com/acefed/strawberryfields-ktor/-/compare/79580db5...9a45a65b
[1.5.0]: https://gitlab.com/acefed/strawberryfields-ktor/-/compare/c8d85349...79580db5
[1.4.0]: https://gitlab.com/acefed/strawberryfields-ktor/-/compare/a6b86917...c8d85349
[1.3.0]: https://gitlab.com/acefed/strawberryfields-ktor/-/compare/d61324f5...a6b86917
[1.2.0]: https://gitlab.com/acefed/strawberryfields-ktor/-/compare/5e0b983d...d61324f5
[1.1.0]: https://gitlab.com/acefed/strawberryfields-ktor/-/compare/7c54a0f8...5e0b983d
