package io.gitlab.acefed.strawberryfieldsktor

import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths
import java.security.KeyFactory
import java.security.MessageDigest
import java.security.PublicKey
import java.security.SecureRandom
import java.security.Signature
import java.security.interfaces.RSAPrivateCrtKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.RSAPublicKeySpec
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.Base64
import java.util.Locale

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.github.cdimascio.dotenv.dotenv

val dotenv = dotenv() {
    ignoreIfMissing = true
}
val objectMapper = ObjectMapper().registerKotlinModule()
val configJson =
    if (!dotenv["CONFIG_JSON"].isNullOrEmpty()) {
        var result = dotenv["CONFIG_JSON"]
        if (result.startsWith("'")) result = result.substring(1)
        if (result.endsWith("'")) result = result.substring(0, result.length - 1)
        result
    } else Files.readAllLines(Paths.get("data/config.json")).joinToString("\n")
val CONFIG = objectMapper.readTree(configJson)
val ME = listOf(
    "<a href=\"https://",
    URI(CONFIG["actor"][0]["me"].textValue()).getHost(),
    "/\" rel=\"me nofollow noopener noreferrer\" target=\"_blank\">",
    "https://",
    URI(CONFIG["actor"][0]["me"].textValue()).getHost(),
    "/",
    "</a>"
).joinToString("")
val PRIVATE_KEY = importPrivateKey(dotenv["PRIVATE_KEY"])
val PUBLIC_KEY = privateKeyToPublicKey(PRIVATE_KEY)
val publicKeyPem = exportPublicKey(PUBLIC_KEY)

fun importPrivateKey(pem: String): RSAPrivateCrtKey {
    val header = "-----BEGIN PRIVATE KEY-----"
    val footer = "-----END PRIVATE KEY-----"
    var b64 = pem
    b64 = b64.replace("\\n", "")
    b64 = b64.replace("\n", "")
    if (b64.startsWith("\"")) b64 = b64.substring(1)
    if (b64.endsWith("\"")) b64 = b64.substring(0, b64.length - 1)
    if (b64.startsWith(header)) b64 = b64.substring(header.length)
    if (b64.endsWith(footer)) b64 = b64.substring(0, b64.length - footer.length)
    val kf = KeyFactory.getInstance("RSA")
    val der = Base64.getDecoder().decode(b64)
    val spec = PKCS8EncodedKeySpec(der)
    val key = kf.generatePrivate(spec) as RSAPrivateCrtKey
    return key
}

fun privateKeyToPublicKey(privateKey: RSAPrivateCrtKey): PublicKey {
    val kf = KeyFactory.getInstance("RSA")
    val spec = RSAPublicKeySpec(privateKey.getModulus(), privateKey.getPublicExponent())
    val publicKey = kf.generatePublic(spec)
    return publicKey
}

fun exportPublicKey(key: PublicKey): String {
    val der = key.getEncoded()
    var b64 = Base64.getEncoder().encodeToString(der)
    var pem = "-----BEGIN PUBLIC KEY-----" + "\n"
    while (b64.length > 64) {
        pem += b64.substring(0, 64) + "\n"
        b64 = b64.substring(64)
    }
    pem += b64.substring(0, b64.length) + "\n"
    pem += "-----END PUBLIC KEY-----" + "\n"
    return pem
}

fun uuidv7(): String {
    val rand = SecureRandom()
    val v = ByteArray(16)
    rand.nextBytes(v)
    val ts = System.currentTimeMillis()
    v[0] = (ts shr 40).toByte()
    v[1] = (ts shr 32).toByte()
    v[2] = (ts shr 24).toByte()
    v[3] = (ts shr 16).toByte()
    v[4] = (ts shr 8).toByte()
    v[5] = ts.toByte()
    v[6] = (v[6].toInt() and 0x0F or 0x70).toByte()
    v[8] = (v[8].toInt() and 0x3F or 0x80).toByte()
    val sb = StringBuilder(32)
    for (b in v) sb.append(String.format("%02x", b))
    return sb.toString()
}

fun talkScript(req: String): String {
    val ts = System.currentTimeMillis()
    if (URI(req).getHost() == "localhost") return "<p>${ts}</p>"
    return listOf(
        "<p>",
        "<a href=\"https://",
        URI(req).getHost(),
        "/\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">",
        URI(req).getHost(),
        "</a>",
        "</p>"
    ).joinToString("")
}

suspend fun getActivity(username: String, hostname: String, req: String): String {
    val t = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH)
        .withZone(ZoneId.of("GMT"))
        .format(Instant.now())
    val sig = Signature.getInstance("SHA256withRSA")
    sig.initSign(PRIVATE_KEY)
    sig.update(listOf(
        "(request-target): get ${URI(req).getPath()}",
        "host: ${URI(req).getHost()}",
        "date: ${t}"
    ).joinToString("\n").toByteArray())
    val b64 = Base64.getEncoder().encodeToString(sig.sign())
    val headers = mapOf(
        "Date" to t,
        "Signature" to listOf(
            "keyId=\"https://${hostname}/u/${username}#Key\"",
            "algorithm=\"rsa-sha256\"",
            "headers=\"(request-target) host date\"",
            "signature=\"${b64}\""
        ).joinToString(","),
        "Accept" to "application/activity+json",
        "Accept-Encoding" to "identity",
        "Cache-Control" to "no-cache",
        "User-Agent" to "StrawberryFields-Ktor/2.0.0 (+https://${hostname}/)"
    )
    val res: HttpResponse = HttpClient(CIO).get(req) {
        headers {
            headers.forEach {
                append(it.key, it.value)
            }
        }
    }
    val status = res.status.value.toString()
    println("GET ${req} ${status}")
    return res.bodyAsText()
}

suspend fun postActivity(username: String, hostname: String, req: String, x: Any) {
    val t = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH)
        .withZone(ZoneId.of("GMT"))
        .format(Instant.now())
    val body = objectMapper.writeValueAsString(x)
    val digest = MessageDigest.getInstance("SHA-256")
    digest.update(body.toByteArray())
    val s256 = Base64.getEncoder().encodeToString(digest.digest())
    val sig = Signature.getInstance("SHA256withRSA")
    sig.initSign(PRIVATE_KEY)
    sig.update(listOf(
        "(request-target): post ${URI(req).getPath()}",
        "host: ${URI(req).getHost()}",
        "date: ${t}",
        "digest: SHA-256=${s256}"
    ).joinToString("\n").toByteArray())
    val b64 = Base64.getEncoder().encodeToString(sig.sign())
    val headers = mapOf(
        "Date" to t,
        "Digest" to "SHA-256=${s256}",
        "Signature" to listOf(
            "keyId=\"https://${hostname}/u/${username}#Key\"",
            "algorithm=\"rsa-sha256\"",
            "headers=\"(request-target) host date digest\"",
            "signature=\"${b64}\""
        ).joinToString(","),
        "Accept" to "application/json",
        "Accept-Encoding" to "gzip",
        "Cache-Control" to "max-age=0",
        "Content-Type" to "application/activity+json",
        "User-Agent" to "StrawberryFields-Ktor/2.0.0 (+https://${hostname}/)"
    )
    println("POST ${req} ${body}")
    HttpClient(CIO).post(req) {
        setBody(body)
        headers {
            headers.forEach {
                append(it.key, it.value)
            }
        }
    }
}

suspend fun acceptFollow(username: String, hostname: String, x: JsonNode, y: JsonNode) {
    val aid = uuidv7()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}",
        "type" to "Accept",
        "actor" to "https://${hostname}/u/${username}",
        "object" to y
    )
    postActivity(username, hostname, x["inbox"].textValue(), body)
}

suspend fun follow(username: String, hostname: String, x: JsonNode) {
    val aid = uuidv7()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}",
        "type" to "Follow",
        "actor" to "https://${hostname}/u/${username}",
        "object" to x["id"].textValue()
    )
    postActivity(username, hostname, x["inbox"].textValue(), body)
}

suspend fun undoFollow(username: String, hostname: String, x: JsonNode) {
    val aid = uuidv7()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}#Undo",
        "type" to "Undo",
        "actor" to "https://${hostname}/u/${username}",
        "object" to mapOf(
            "id" to "https://${hostname}/u/${username}/s/${aid}",
            "type" to "Follow",
            "actor" to "https://${hostname}/u/${username}",
            "object" to x["id"].textValue()
        )
    )
    postActivity(username, hostname, x["inbox"].textValue(), body)
}

suspend fun like(username: String, hostname: String, x: JsonNode, y: JsonNode) {
    val aid = uuidv7()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}",
        "type" to "Like",
        "actor" to "https://${hostname}/u/${username}",
        "object" to x["id"].textValue()
    )
    postActivity(username, hostname, y["inbox"].textValue(), body)
}

suspend fun undoLike(username: String, hostname: String, x: JsonNode, y: JsonNode) {
    val aid = uuidv7()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}#Undo",
        "type" to "Undo",
        "actor" to "https://${hostname}/u/${username}",
        "object" to mapOf(
            "id" to "https://${hostname}/u/${username}/s/${aid}",
            "type" to "Like",
            "actor" to "https://${hostname}/u/${username}",
            "object" to x["id"].textValue()
        )
    )
    postActivity(username, hostname, y["inbox"].textValue(), body)
}

suspend fun announce(username: String, hostname: String, x: JsonNode, y: JsonNode) {
    val aid = uuidv7()
    val t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}/activity",
        "type" to "Announce",
        "actor" to "https://${hostname}/u/${username}",
        "published" to t,
        "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
        "cc" to listOf("https://${hostname}/u/${username}/followers"),
        "object" to x["id"].textValue()
    )
    postActivity(username, hostname, y["inbox"].textValue(), body)
}

suspend fun undoAnnounce(username: String, hostname: String, x: JsonNode, y: JsonNode, z: String) {
    val aid = uuidv7()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}#Undo",
        "type" to "Undo",
        "actor" to "https://${hostname}/u/${username}",
        "object" to mapOf(
            "id" to "${z}/activity",
            "type" to "Announce",
            "actor" to "https://${hostname}/u/${username}",
            "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
            "cc" to listOf("https://${hostname}/u/${username}/followers"),
            "object" to x["id"].textValue()
        )
    )
    postActivity(username, hostname, y["inbox"].textValue(), body)
}

suspend fun createNote(username: String, hostname: String, x: JsonNode, y: String) {
    val aid = uuidv7()
    val t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}/activity",
        "type" to "Create",
        "actor" to "https://${hostname}/u/${username}",
        "published" to t,
        "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
        "cc" to listOf("https://${hostname}/u/${username}/followers"),
        "object" to mapOf(
            "id" to "https://${hostname}/u/${username}/s/${aid}",
            "type" to "Note",
            "attributedTo" to "https://${hostname}/u/${username}",
            "content" to talkScript(y),
            "url" to "https://${hostname}/u/${username}/s/${aid}",
            "published" to t,
            "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
            "cc" to listOf("https://${hostname}/u/${username}/followers")
        )
    )
    postActivity(username, hostname, x["inbox"].textValue(), body)
}

suspend fun createNoteImage(username: String, hostname: String, x: JsonNode, y: String, z: String) {
    val aid = uuidv7()
    val t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}/activity",
        "type" to "Create",
        "actor" to "https://${hostname}/u/${username}",
        "published" to t,
        "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
        "cc" to listOf("https://${hostname}/u/${username}/followers"),
        "object" to mapOf(
            "id" to "https://${hostname}/u/${username}/s/${aid}",
            "type" to "Note",
            "attributedTo" to "https://${hostname}/u/${username}",
            "content" to talkScript("https://localhost"),
            "url" to "https://${hostname}/u/${username}/s/${aid}",
            "published" to t,
            "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
            "cc" to listOf("https://${hostname}/u/${username}/followers"),
            "attachment" to listOf(
                mapOf(
                    "type" to "Image",
                    "mediaType" to z,
                    "url" to y
                )
            )
        )
    )
    postActivity(username, hostname, x["inbox"].textValue(), body)
}

suspend fun createNoteMention(username: String, hostname: String, x: JsonNode, y: JsonNode, z: String) {
    val aid = uuidv7()
    val t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString()
    val at = "@${y["preferredUsername"].textValue()}@${URI(y["inbox"].textValue()).getHost()}"
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "https://${hostname}/u/${username}/s/${aid}/activity",
        "type" to "Create",
        "actor" to "https://${hostname}/u/${username}",
        "published" to t,
        "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
        "cc" to listOf("https://${hostname}/u/${username}/followers"),
        "object" to mapOf(
            "id" to "https://${hostname}/u/${username}/s/${aid}",
            "type" to "Note",
            "attributedTo" to "https://${hostname}/u/${username}",
            "inReplyTo" to x["id"].textValue(),
            "content" to talkScript(z),
            "url" to "https://${hostname}/u/${username}/s/${aid}",
            "published" to t,
            "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
            "cc" to listOf("https://${hostname}/u/${username}/followers"),
            "tag" to listOf(
                mapOf(
                    "type" to "Mention",
                    "name" to at
                )
            )
        )
    )
    postActivity(username, hostname, y["inbox"].textValue(), body)
}

suspend fun createNoteHashtag(username: String, hostname: String, x: JsonNode, y: String, z: String) {
    val aid = uuidv7()
    val t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString()
    val body = mapOf(
        "@context" to listOf(
            "https://www.w3.org/ns/activitystreams",
            mapOf("Hashtag" to "as:Hashtag")
        ),
        "id" to "https://${hostname}/u/${username}/s/${aid}/activity",
        "type" to "Create",
        "actor" to "https://${hostname}/u/${username}",
        "published" to t,
        "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
        "cc" to listOf("https://${hostname}/u/${username}/followers"),
        "object" to mapOf(
            "id" to "https://${hostname}/u/${username}/s/${aid}",
            "type" to "Note",
            "attributedTo" to "https://${hostname}/u/${username}",
            "content" to talkScript(y),
            "url" to "https://${hostname}/u/${username}/s/${aid}",
            "published" to t,
            "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
            "cc" to listOf("https://${hostname}/u/${username}/followers"),
            "tag" to listOf(
                mapOf(
                    "type" to "Hashtag",
                    "name" to "#${z}"
                )
            )
        )
    )
    postActivity(username, hostname, x["inbox"].textValue(), body)
}

suspend fun updateNote(username: String, hostname: String, x: JsonNode, y: String) {
    val t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString()
    val ts = System.currentTimeMillis()
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "${y}#${ts}",
        "type" to "Update",
        "actor" to "https://${hostname}/u/${username}",
        "published" to t,
        "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
        "cc" to listOf("https://${hostname}/u/${username}/followers"),
        "object" to mapOf(
            "id" to y,
            "type" to "Note",
            "attributedTo" to "https://${hostname}/u/${username}",
            "content" to talkScript("https://localhost"),
            "url" to y,
            "updated" to t,
            "to" to listOf("https://www.w3.org/ns/activitystreams#Public"),
            "cc" to listOf("https://${hostname}/u/${username}/followers")
        )
    )
    postActivity(username, hostname, x["inbox"].textValue(), body)
}

suspend fun deleteTombstone(username: String, hostname: String, x: JsonNode, y: String) {
    val body = mapOf(
        "@context" to "https://www.w3.org/ns/activitystreams",
        "id" to "${y}#Delete",
        "type" to "Delete",
        "actor" to "https://${hostname}/u/${username}",
        "object" to mapOf(
            "id" to y,
            "type" to "Tombstone"
        )
    )
    postActivity(username, hostname, x["inbox"].textValue(), body)
}

fun main(args: Array<String>) {
    io.ktor.server.cio.EngineMain.main(args)
}

fun Application.module() {
    install(StatusPages) {
        status(HttpStatusCode.NotFound) { call, _ ->
            call.respondText("404 Not Found", null, HttpStatusCode.NotFound)
        }
        exception<Throwable> { call, _ ->
            call.respondText("Internal Server Error", null, HttpStatusCode.InternalServerError)
        }
    }
    routing {
        staticResources("/", "static")

        get("/") {
            call.respondText("StrawberryFields Ktor")
        }

        get("/about") {
            call.respondText("About: Blank")
        }

        get("/u/{username}") {
            val username = call.parameters["username"]
            val hostname = URI(CONFIG["origin"].textValue()).getHost()
            val acceptHeaderField = call.request.header(HttpHeaders.Accept) ?: ""
            var hasType = false
            if (username != CONFIG["actor"][0]["preferredUsername"].textValue()) {
                return@get call.respond(HttpStatusCode.NotFound)
            }
            if (acceptHeaderField.contains("application/activity+json")) hasType = true
            if (acceptHeaderField.contains("application/ld+json")) hasType = true
            if (acceptHeaderField.contains("application/json")) hasType = true
            if (!hasType) {
                val body = "${username}: ${CONFIG["actor"][0]["name"].textValue()}"
                val headers = mapOf(
                    HttpHeaders.CacheControl to "public, max-age=${CONFIG["ttl"]?.asText()}, must-revalidate",
                    HttpHeaders.Vary to "Accept, Accept-Encoding"
                )
                headers.forEach {
                    call.response.headers.append(it.key, it.value)
                }
                return@get call.respondText(body)
            }
            val body = objectMapper.writeValueAsString(
                mapOf(
                    "@context" to listOf(
                        "https://www.w3.org/ns/activitystreams",
                        "https://w3id.org/security/v1",
                        mapOf(
                            "schema" to "https://schema.org/",
                            "PropertyValue" to "schema:PropertyValue",
                            "value" to "schema:value",
                            "Key" to "sec:Key"
                        )
                    ),
                    "id" to "https://${hostname}/u/${username}",
                    "type" to "Person",
                    "inbox" to "https://${hostname}/u/${username}/inbox",
                    "outbox" to "https://${hostname}/u/${username}/outbox",
                    "following" to "https://${hostname}/u/${username}/following",
                    "followers" to "https://${hostname}/u/${username}/followers",
                    "preferredUsername" to username,
                    "name" to CONFIG["actor"][0]["name"].textValue(),
                    "summary" to "<p>2.0.0</p>",
                    "url" to "https://${hostname}/u/${username}",
                    "endpoints" to mapOf("sharedInbox" to "https://${hostname}/u/${username}/inbox"),
                    "attachment" to listOf(
                        mapOf(
                            "type" to "PropertyValue",
                            "name" to "me",
                            "value" to ME
                        )
                    ),
                    "icon" to mapOf(
                        "type" to "Image",
                        "mediaType" to "image/png",
                        "url" to "https://${hostname}/static/${username}u.png"
                    ),
                    "image" to mapOf(
                        "type" to "Image",
                        "mediaType" to "image/png",
                        "url" to "https://${hostname}/static/${username}s.png"
                    ),
                    "publicKey" to mapOf(
                        "id" to "https://${hostname}/u/${username}#Key",
                        "type" to "Key",
                        "owner" to "https://${hostname}/u/${username}",
                        "publicKeyPem" to publicKeyPem
                    )
                )
            )
            val headers = mapOf(
                HttpHeaders.CacheControl to "public, max-age=${CONFIG["ttl"]?.asText()}, must-revalidate",
                HttpHeaders.Vary to "Accept, Accept-Encoding"
            )
            headers.forEach {
                call.response.headers.append(it.key, it.value)
            }
            call.respondText(body, ContentType("application", "activity+json"))
        }

        get("/u/{username}/inbox") {
            call.respond(HttpStatusCode.MethodNotAllowed)
        }
        post("/u/{username}/inbox") {
            val username = call.parameters["username"] as String
            val hostname = URI(CONFIG["origin"].textValue()).getHost()
            val contentTypeHeaderField = call.request.header(HttpHeaders.ContentType) ?: ""
            var hasType = false
            val y = call.receiveText()
            val ny = objectMapper.readTree(y)
            var t = ny["type"]?.textValue() ?: ""
            val aid = ny["id"]?.textValue() ?: ""
            val atype = ny["type"]?.textValue() ?: ""
            if (aid.length > 1024 || atype.length > 64) return@post call.respond(HttpStatusCode.BadRequest)
            println("INBOX ${aid} ${atype}")
            if (username != CONFIG["actor"][0]["preferredUsername"].textValue()) {
                return@post call.respond(HttpStatusCode.NotFound)
            }
            if (contentTypeHeaderField.contains("application/activity+json")) hasType = true
            if (contentTypeHeaderField.contains("application/ld+json")) hasType = true
            if (contentTypeHeaderField.contains("application/json")) hasType = true
            if (!hasType) return@post call.respond(HttpStatusCode.BadRequest)
            if (call.request.header("Digest").isNullOrEmpty()) return@post call.respond(HttpStatusCode.BadRequest)
            if (call.request.header("Signature").isNullOrEmpty()) return@post call.respond(HttpStatusCode.BadRequest)
            if (t == "Accept" || t == "Reject" || t == "Add") return@post call.respond(HttpStatusCode.OK)
            if (t == "Remove" || t == "Like" || t == "Announce") return@post call.respond(HttpStatusCode.OK)
            if (t == "Create" || t == "Update" || t == "Delete") return@post call.respond(HttpStatusCode.OK)
            if (t == "Follow") {
                if (URI(ny["actor"]?.textValue() ?: "").getScheme() != "https") {
                    return@post call.respond(HttpStatusCode.BadRequest)
                }
                val x = getActivity(username, hostname, ny["actor"].textValue())
                if (x.isNullOrEmpty()) return@post call.respond(HttpStatusCode.InternalServerError)
                val nx = objectMapper.readTree(x)
                acceptFollow(username, hostname, nx, ny)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "Undo") {
                val nz = ny.withObject("object") as ObjectNode
                t = nz["type"]?.textValue() ?: ""
                if (t == "Accept" || t == "Like" || t == "Announce") return@post call.respond(HttpStatusCode.OK)
                if (t == "Follow") {
                    if (URI(ny["actor"]?.textValue() ?: "").getScheme() != "https") {
                        return@post call.respond(HttpStatusCode.BadRequest)
                    }
                    val x = getActivity(username, hostname, ny["actor"].textValue())
                    if (x.isNullOrEmpty()) return@post call.respond(HttpStatusCode.InternalServerError)
                    val nx = objectMapper.readTree(x)
                    acceptFollow(username, hostname, nx, nz)
                    return@post call.respond(HttpStatusCode.OK)
                }
            }
            call.respond(HttpStatusCode.InternalServerError)
        }

        post("/u/{username}/outbox") {
            call.respond(HttpStatusCode.MethodNotAllowed)
        }
        get("/u/{username}/outbox") {
            val username = call.parameters["username"]
            val hostname = URI(CONFIG["origin"].textValue()).getHost()
            if (username != CONFIG["actor"][0]["preferredUsername"].textValue()) {
                return@get call.respond(HttpStatusCode.NotFound)
            }
            val body = objectMapper.writeValueAsString(
                mapOf(
                    "@context" to "https://www.w3.org/ns/activitystreams",
                    "id" to "https://${hostname}/u/${username}/outbox",
                    "type" to "OrderedCollection",
                    "totalItems" to 0
                )
            )
            call.respondText(body, ContentType("application", "activity+json"))
        }

        get("/u/{username}/following") {
            val username = call.parameters["username"]
            val hostname = URI(CONFIG["origin"].textValue()).getHost()
            if (username != CONFIG["actor"][0]["preferredUsername"].textValue()) {
                return@get call.respond(HttpStatusCode.NotFound)
            }
            val body = objectMapper.writeValueAsString(
                mapOf(
                    "@context" to "https://www.w3.org/ns/activitystreams",
                    "id" to "https://${hostname}/u/${username}/following",
                    "type" to "OrderedCollection",
                    "totalItems" to 0
                )
            )
            call.respondText(body, ContentType("application", "activity+json"))
        }

        get("/u/{username}/followers") {
            val username = call.parameters["username"]
            val hostname = URI(CONFIG["origin"].textValue()).getHost()
            if (username != CONFIG["actor"][0]["preferredUsername"].textValue()) {
                return@get call.respond(HttpStatusCode.NotFound)
            }
            val body = objectMapper.writeValueAsString(
                mapOf(
                    "@context" to "https://www.w3.org/ns/activitystreams",
                    "id" to "https://${hostname}/u/${username}/followers",
                    "type" to "OrderedCollection",
                    "totalItems" to 0
                )
            )
            call.respondText(body, ContentType("application", "activity+json"))
        }

        post("/s/{secret}/u/{username}") {
            val username = call.parameters["username"] as String
            val hostname = URI(CONFIG["origin"].textValue()).getHost()
            val body = call.receiveText()
            val send = objectMapper.readTree(body)
            val t = send["type"]?.textValue() ?: ""
            if (username != CONFIG["actor"][0]["preferredUsername"].textValue()) {
                return@post call.respond(HttpStatusCode.NotFound)
            }
            if (call.parameters["secret"].isNullOrEmpty() || call.parameters["secret"] == "-") {
                return@post call.respond(HttpStatusCode.NotFound)
            }
            if (call.parameters["secret"] != dotenv["SECRET"]) return@post call.respond(HttpStatusCode.NotFound)
            if (URI(send["id"]?.textValue() ?: "").getScheme() != "https") {
                return@post call.respond(HttpStatusCode.BadRequest)
            }
            val x = getActivity(username, hostname, send["id"].textValue())
            if (x.isNullOrEmpty()) return@post call.respond(HttpStatusCode.InternalServerError)
            val nx = objectMapper.readTree(x)
            val aid = nx["id"]?.textValue() ?: ""
            val atype = nx["type"]?.textValue() ?: ""
            if (aid.length > 1024 || atype.length > 64) return@post call.respond(HttpStatusCode.BadRequest)
            if (t == "follow") {
                follow(username, hostname, nx)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "undo_follow") {
                undoFollow(username, hostname, nx)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "like") {
                if (URI(nx["attributedTo"]?.textValue() ?: "").getScheme() != "https") {
                    return@post call.respond(HttpStatusCode.BadRequest)
                }
                val y = getActivity(username, hostname, nx["attributedTo"].textValue())
                if (y.isNullOrEmpty()) return@post call.respond(HttpStatusCode.InternalServerError)
                val ny = objectMapper.readTree(y)
                like(username, hostname, nx, ny)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "undo_like") {
                if (URI(nx["attributedTo"]?.textValue() ?: "").getScheme() != "https") {
                    return@post call.respond(HttpStatusCode.BadRequest)
                }
                val y = getActivity(username, hostname, nx["attributedTo"].textValue())
                if (y.isNullOrEmpty()) return@post call.respond(HttpStatusCode.InternalServerError)
                val ny = objectMapper.readTree(y)
                undoLike(username, hostname, nx, ny)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "announce") {
                if (URI(nx["attributedTo"]?.textValue() ?: "").getScheme() != "https") {
                    return@post call.respond(HttpStatusCode.BadRequest)
                }
                val y = getActivity(username, hostname, nx["attributedTo"].textValue())
                if (y.isNullOrEmpty()) return@post call.respond(HttpStatusCode.InternalServerError)
                val ny = objectMapper.readTree(y)
                announce(username, hostname, nx, ny)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "undo_announce") {
                if (URI(nx["attributedTo"]?.textValue() ?: "").getScheme() != "https") {
                    return@post call.respond(HttpStatusCode.BadRequest)
                }
                val y = getActivity(username, hostname, nx["attributedTo"].textValue())
                if (y.isNullOrEmpty()) return@post call.respond(HttpStatusCode.InternalServerError)
                val ny = objectMapper.readTree(y)
                val z =
                    send["url"]?.textValue()?.takeIf { it.isNotEmpty() }
                        ?: "https://${hostname}/u/${username}/s/00000000000000000000000000000000"
                if (URI(z).getScheme() != "https") return@post call.respond(HttpStatusCode.BadRequest)
                undoAnnounce(username, hostname, nx, ny, z)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "create_note") {
                val y = send["url"]?.textValue()?.takeIf { it.isNotEmpty() } ?: "https://localhost"
                if (URI(y).getScheme() != "https") return@post call.respond(HttpStatusCode.BadRequest)
                createNote(username, hostname, nx, y)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "create_note_image") {
                val y =
                    send["url"]?.textValue()?.takeIf { it.isNotEmpty() } ?: "https://${hostname}/static/logo.png"
                if (URI(y).getScheme() != "https" || URI(y).getHost() != hostname) {
                    return@post call.respond(HttpStatusCode.BadRequest)
                }
                val z =
                    if (y.endsWith(".jpg") || y.endsWith(".jpeg")) "image/jpeg"
                    else if (y.endsWith(".svg")) "image/svg+xml"
                    else if (y.endsWith(".gif")) "image/gif"
                    else if (y.endsWith(".webp")) "image/webp"
                    else if (y.endsWith(".avif")) "image/avif"
                    else "image/png"
                createNoteImage(username, hostname, nx, y, z)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "create_note_mention") {
                if (URI(nx["attributedTo"]?.textValue() ?: "").getScheme() != "https") {
                    return@post call.respond(HttpStatusCode.BadRequest)
                }
                val y = getActivity(username, hostname, nx["attributedTo"].textValue())
                if (y.isNullOrEmpty()) return@post call.respond(HttpStatusCode.InternalServerError)
                val ny = objectMapper.readTree(y)
                val z = send["url"]?.textValue()?.takeIf { it.isNotEmpty() } ?: "https://localhost"
                if (URI(z).getScheme() != "https") return@post call.respond(HttpStatusCode.BadRequest)
                createNoteMention(username, hostname, nx, ny, z)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "create_note_hashtag") {
                val y = send["url"]?.textValue()?.takeIf { it.isNotEmpty() } ?: "https://localhost"
                if (URI(y).getScheme() != "https") return@post call.respond(HttpStatusCode.BadRequest)
                val z = send["tag"]?.textValue()?.takeIf { it.isNotEmpty() } ?: "Hashtag"
                createNoteHashtag(username, hostname, nx, y, z)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "update_note") {
                val y =
                    send["url"]?.textValue()?.takeIf { it.isNotEmpty() }
                        ?: "https://${hostname}/u/${username}/s/00000000000000000000000000000000"
                if (URI(y).getScheme() != "https") return@post call.respond(HttpStatusCode.BadRequest)
                updateNote(username, hostname, nx, y)
                return@post call.respond(HttpStatusCode.OK)
            }
            if (t == "delete_tombstone") {
                val y =
                    send["url"]?.textValue()?.takeIf { it.isNotEmpty() }
                        ?: "https://${hostname}/u/${username}/s/00000000000000000000000000000000"
                if (URI(y).getScheme() != "https") return@post call.respond(HttpStatusCode.BadRequest)
                deleteTombstone(username, hostname, nx, y)
                return@post call.respond(HttpStatusCode.OK)
            }
            println("TYPE ${aid} ${atype}")
            call.respond(HttpStatusCode.OK)
        }

        get("/.well-known/nodeinfo") {
            val hostname = URI(CONFIG["origin"].textValue()).getHost()
            val body = objectMapper.writeValueAsString(
                mapOf(
                    "links" to listOf(
                        mapOf(
                            "rel" to "http://nodeinfo.diaspora.software/ns/schema/2.0",
                            "href" to "https://${hostname}/nodeinfo/2.0.json"
                        ),
                        mapOf(
                            "rel" to "http://nodeinfo.diaspora.software/ns/schema/2.1",
                            "href" to "https://${hostname}/nodeinfo/2.1.json"
                        )
                    )
                )
            )
            val headers = mapOf(
                HttpHeaders.CacheControl to "public, max-age=${CONFIG["ttl"]?.asText()}, must-revalidate",
                HttpHeaders.Vary to "Accept, Accept-Encoding"
            )
            headers.forEach {
                call.response.headers.append(it.key, it.value)
            }
            call.respondText(body, ContentType.Application.Json)
        }

        get("/.well-known/webfinger") {
            val username = CONFIG["actor"][0]["preferredUsername"].textValue()
            val hostname = URI(CONFIG["origin"].textValue()).getHost()
            var p443 = "https://${hostname}:443/"
            var resource = call.request.queryParameters["resource"].toString()
            var hasResource = false
            if (resource.startsWith(p443)) resource = "https://${hostname}/${resource.substring(p443.length)}"
            if (resource == "acct:${username}@${hostname}") hasResource = true
            if (resource == "mailto:${username}@${hostname}") hasResource = true
            if (resource == "https://${hostname}/@${username}") hasResource = true
            if (resource == "https://${hostname}/u/${username}") hasResource = true
            if (resource == "https://${hostname}/user/${username}") hasResource = true
            if (resource == "https://${hostname}/users/${username}") hasResource = true
            if (!hasResource) return@get call.respond(HttpStatusCode.NotFound)
            val body = objectMapper.writeValueAsString(
                mapOf(
                    "subject" to "acct:${username}@${hostname}",
                    "aliases" to listOf(
                        "mailto:${username}@${hostname}",
                        "https://${hostname}/@${username}",
                        "https://${hostname}/u/${username}",
                        "https://${hostname}/user/${username}",
                        "https://${hostname}/users/${username}"
                    ),
                    "links" to listOf(
                        mapOf(
                            "rel" to "self",
                            "type" to "application/activity+json",
                            "href" to "https://${hostname}/u/${username}"
                        ),
                        mapOf(
                            "rel" to "http://webfinger.net/rel/avatar",
                            "type" to "image/png",
                            "href" to "https://${hostname}/static/${username}u.png"
                        ),
                        mapOf(
                            "rel" to "http://webfinger.net/rel/profile-page",
                            "type" to "text/plain",
                            "href" to "https://${hostname}/u/${username}"
                        )
                    )
                )
            )
            val headers = mapOf(
                HttpHeaders.CacheControl to "public, max-age=${CONFIG["ttl"]?.asText()}, must-revalidate",
                HttpHeaders.Vary to "Accept, Accept-Encoding"
            )
            headers.forEach {
                call.response.headers.append(it.key, it.value)
            }
            call.respondText(body, ContentType("application", "jrd+json"))
        }

        get("/@") {
            call.respondRedirect("/")
        }
        get("/u") {
            call.respondRedirect("/")
        }
        get("/user") {
            call.respondRedirect("/")
        }
        get("/users") {
            call.respondRedirect("/")
        }

        get("/users/{username}") {
            call.respondRedirect("/u/${call.parameters["username"]}")
        }
        get("/user/{username}") {
            call.respondRedirect("/u/${call.parameters["username"]}")
        }
        get("/@{username}") {
            call.respondRedirect("/u/${call.parameters["username"]}")
        }
    }
}
